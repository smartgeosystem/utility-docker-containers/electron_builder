FROM electronuserland/builder:16-wine-11.23

ARG TZ=UTC

RUN set -ex \
	&& ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
	# Update packages
    && apt update -yqq \
    # && apt upgrade -yqq \
    # Install system deps
    && apt install -yqq --no-install-recommends \
    # Upload&Utils
    curl gettext-base jq python3-pkg-resources \
    python3-pip python3-setuptools \
    # install nexus3-cli
    && pip3 install nexus3-cli \
    && apt remove -yqq  python3-pip python3-setuptools \
    && apt autoremove -yqq \
    # Clean apt
    && rm -rf \
      /var/lib/apt/lists/* \
      /tmp/* \
      /var/tmp/* \
      /usr/share/man \
      /usr/share/doc \
      /usr/share/doc-base \
    # Add telegram notifier
    && curl --output /usr/bin/telegram_notifier https://gitlab.com/smartgeosystem/utilities/telegram_notifier/-/package_files/4784406/download \
    && chmod +x /usr/bin/telegram_notifier
